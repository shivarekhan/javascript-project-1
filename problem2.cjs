function problem2(inventory) {
    if(!Array.isArray(inventory) || inventory.length==0){
        return [];}
    else{
        return inventory[inventory.length-1]; // 0 base indexing.
    }
}
module.exports = problem2;