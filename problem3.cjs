function cmp(a,b){
    if(a.car_model < b.car_model)return -1;
    else{
        return 1;
    }
}
function problem3(inventory) {
    if(!Array.isArray(inventory) || inventory.length==0){
        return [];
    }
    inventory.sort(cmp);
    return inventory;
}
module.exports = problem3;