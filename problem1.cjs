function problem1(inventory, index) {
    if (inventory === undefined ||  typeof index !== 'number' || !Array.isArray(inventory)) return [];
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id == index) {
            return inventory[i];
        }
    }
    return [];
}

module.exports = problem1;