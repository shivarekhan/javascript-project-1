function problem5(outputFromProblem4){
    if(outputFromProblem4.length ===0 || outputFromProblem4 === undefined){
        return [];
    }
    let arrOfYear = [];
    for(let i=0 ; i<outputFromProblem4.length ; i++){
        if(outputFromProblem4[i] < 2000){
            arrOfYear.push(outputFromProblem4[i]);
        }
    }
    return arrOfYear;
}
module.exports = problem5;