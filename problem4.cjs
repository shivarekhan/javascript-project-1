function problem4(inventory) {
    if (!Array.isArray(inventory) || inventory.length === 0) {
        return [];
    }
    let year = [];
    for (let i = 0; i < inventory.length; i++) {
        year[i] = inventory[i].car_year;
    }
    return year;
}

module.exports = problem4;